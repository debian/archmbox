Archmbox was designed and implemented by:

Alessandro Dotti Contra <adotti@users.sourceforge.net>

Parts of the code were contributed by:

Alex Aminoff <alex_aminoff@alum.mit.edu>
	improved regular expressions for finding the beginning of a message

Brian Medley <bpm@4321.tv>
	support for lsof as a replacement for fuser

Buck Holsinger <buckh@rrinc.com>
	support for option -m 
	support for option --nosymlink
	support for option --ignore

Davor Ocelic <docelic@teleport.medri.hr>
	support for options -l, -r, -v, -x
	engine to support regular expressions based archiving
	support for probing helpers path at installation time in the original
	install script

Fabrice Noilhan <fabrice-archmbox@noilhan.com>
	support for status header in regexp based archiving

Jayanth Varma <jrvarma@iimahd.ernet.in>
	support for copy mode (option -y)
	support for option --omit-prefix
	use of no extension for the archive mailbox
	use of time information to refine archiving
	mozilla's mailbox name normalization

Juergen Edner <jurgen.edner@telejeck.de>
	revised code for mbx mailboxes support (mailutil converter)

Laurent Cheylus <foxy@free.fr>
	use of 'Date:' header to age messages

Nicolas Ecarnot <nicolas.ecarnot@accim.com>
	tests for shell helpers return code
	support for temporary working directory as a runtime option
	(option -t)
	support for fstat as a replacement for fuser

Paco Regodon <regodon@itcom.com>
	support for concurrent mailbox access
	support for handling mailboxes ownership and permissions correctly

Scott Thompson <sco@openface.ca>
	support for kill mode (option -k)

Juergen Descher <jhdl@gmx.net>
	support for logical and in regular expressions based archiving
	correct evaluation of leap years
	improved handling of temporary working directory
	support for custom archive mailbox name
	general improvements to code and documentation

FreeBSD port is maintained by Talal Al-Dik <tad@vif.com>
OpenDarwin port is maintained by Markus Weissmann <mww@opendarwin.org>
Debian package is maintained by Alberto Furia <straluna@email.it>

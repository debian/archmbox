release 4.10.0

* option to customize the archive mailbox name was added (thanks to
  Juergen Descher).
* support for logical and in regular expressions based archiving was
  added (thanks to Juergen Descher).
* code cleanups and other improvents were performed.

release 4.9.0

* some mailboxes/directories can be skipped, using a regular
  expression, while archiving (Thanks to Buck Holsinger).
* the TMP_DIR environment variable was renamed to ARCHMBOX_TMP_DIR.
* the list mode's output is no longer generated using perl formats.
  This fixes some problems that may sometimes arise when archmbox is
  piped trough a pager (bug 1115657 reported by Buck Holsinger).
* option -n was removed.

release 4.8.0

* option to use the "Date:" header to age messages was added (thanks
  to Laurent Cheylus).
* minor code cleanups and output improvements.

release 4.7.0:

* mailboxes smaller than a specified size can be exempt from
  archiving (Thanks to Buck Holsinger).
* symbolic links can be skipped (Thanks to Buck Holsinger).
* options -t and -p now require full path (bug 961406 reported by Buck
  Holsinger).
* "Folder internal data message" is no longer counted.
* mailbox related warnings can be skipped.
* the name of the archive mailbox is printed (Thanks to Terry Brown).
* an overall summary of the archiving process is available.
* other bug fixes and code improvements (Thanks to Wojcieck Scigala and
  others).

release 4.6.0:

* temporary working mailboxes are always removed if archmbox exits
  abnormally.
* a warn is issued when a mailbox is empty (Thanks to Juergen Edner).
* "Folder internal data" message is always skipped during archiving.
* few things were tweaked to allow archmbox to run under CygWin on
  Windows platforms (Thanks to Jayanth Varma).
* fstat and lsof support was added. Those binaries can now be used as
  a replacement for fuser (Thanks to Nicolas Ecarnot and Brian Medley).
* regexp archiving can be performed on any header.
* flagged and unread messages can be skipped.
* a new copy mode was added. Messages can be copied from the source
  mailbox without actually modify it (Thanks to Jayanth Varma).
* a configurable prefix can be omitted from the mailbox name when full
  name is requested for the archive mailbox (Thanks to Jayanth Varma).
* archiving can be refined specifying a time value (Thanks to Jayanth
  Varma).
* no extension can be requested for the archive mailbox if desired
  (Thanks to Jayanth Varma).
* some bug fixes and code cleanup.

release 4.5.2:

* option -t was added; this option lets you specify the temporary
  working directory to use and overrides the value specified at
  installation time or the one hard coded in the script (Thanks to
  Nicolas Ecarnot).
* more checks for the return codes of shell helpers were added. This
  fixes bug 901359 (reported and fixed by Nicolas Ecarnot).

release 4.5.1:

* a bug which causes messages to be skipped during listing/archiving
  process under some circumstances was fixed (bug 872838: 
  reported by Blake Gonzales)

release 4.5.0:

* support for mbx mailboxes has been added. --format option can be
  used to specify which type of mailbox must be processed (Thanks to
  Juergen Edner).
* a working directory is now used to handle temporary mailboxes before
  real actions take place. This can be configured at installation
  time.
* mailboxes must be specified using full path.

release 4.4.2:

* mailbox names containing spaces are now handled correctly
* the mtime of the mailbox is not changed if no message was
  archived (thanks to Fabrice Noilhan)

release 4.4.1:

* a weird bug which caused mailboxes corruption was fixed

release 4.4.0:

* support for status header in regexp based archiving was added
  (Thanks to Fabrice Noilhan)
* an harmless bug which causes archmbox to die if a mailbox is in
  use when multiple mailboxes are specified was fixed
* output was improved; total mailbox size and total saved space
  are printed along with all the usual informations

release 4.3.0:

* running mode must be specified, as archmbox doesn't consider
  "archive" mode as default any longer
* support for kill mode was added. With this mode, messages are
  simply deleted from the original mailbox(es) (Thanks to Scott
  Thompson)
* mailboxes handling was improved; binary files are skipped
* help and documentation were improved
* minor bugs were fixed

release 4.2.0:

* support for multiple regexp archiving was added. If -x is
  specified more than once, the message is regexp matched against
  the given rules, and if it satisfies any, will be archived
* minor changes to output

release 4.1.0:

* an harmless warning issued if any header of the email message
  is undefined was fixed
* full name for archive mailbox is used if recursion is
  specified
* reports for list mode were slightly improved

release 4.0.0:

* code was cleaned and improved
* install script was removed. A configure script and a makefile
  are used instead
* option --probe-helpers was removed as it's no longer needed

release 3.1.1:

* a bug in options handling was fixed. Option -r is now handled
  correctly
  
release 3.1.0:

* recursive archiving of a tree of mailboxes is now supported
* the correct path for shell helpers is now probed by the install
  script at installation time (Thanks to Davor Ocelic)

release 3.0.4:

* option -s (soft check) was removed since it's no longer needed
* a minor bug which causes the script to crash if the beginning
  line of a message contains time zone information was fixed
  (Reported and fixed by Alex Aminoff)
	
release 3.0.3:

* a rare bug which may leads to messages loss was fixed
  (Reported by Alex Aminoff)

release 3.0.2:

* regular expressions archiving support for fields "cc" and 
  "bcc" was added
* man page was reformatted
		
release 3.0.1:	

* an harmless bug in the handler of compressed archived mailbox 
  was fixed
* output formats for list mode were improved
		
release 3.0.0:

* support for multiple mailboxes was added

release 2.1.0:

* code was cleaned
* shell helpers handling was improved
* messages archiving can be explicitly forced (-a)
* bzip2 compression support was added
* man page was added
		
release 2.0.1:

* a stupid bug which prevents custom extension of archive
  mailbox to be handled correctly was fixed.

release 2.0.0:

(All improvements contributed by Davor Ocelic)
* script is now called archmbox , instead of archmbox.pl.
* version can now only be obtained with --version.
* locations of helper apps can be detected at runtime.
* option -l to only list messages which will be archived was
  added.
* option -v now means verbosity (1 or 2, for 1 or 2 lines of 
  output per message); level 1 is the default.
* option -r which reverses the sense of date offset was added
  (see README).
* use -n to avoid printing page header when in list mode (-l)
* regular expression based messages archiving (beside the
  offset and date) was added. You can match messages by
  regexp rules on arbitrary field. Getoptions routine does
  not support stacked variables, so you cannot do regexp on
  multiple fields at once (regexp is *case insensitive*, and
  fields can be: from, envelope, sender, messageid, date,
  xmailer, to, subject or type).
* we no more link /usr/bin/archmbox to
  /usr/local/bin/archmbox at installation point.

release 1.0.0:

* A backup of the mailbox can be created before running the
  script.
* The full path of the mailbox can be used for archive mailbox.
* Archive mailbox can be compressed on the fly.
* A loose check on messages beginning can be performed.
* Install script and documentation were improved.
		
release 0.3.2:

* A bug which led to a wrong recognition of the beginning
  of a message was fixed.
* The default location of shell helpers can be configured.

release 0.3.1:

* A bug which prevents a correct handling of archive 
  mailbox's path was fixed.

release 0.3.0:

* Support for long options was added.
* Mailbox ownership and permissions are kept after script runs
  (Thanks to Paco Regodon)
* Archmbox aborts if the mailbox is in use or has been changed
  during execution.
  (Thanks to Paco Regodon)
		  
release 0.2.1:

* Part of the code was rewritten to fix a problem under
  *BSD systems (Reported by Bob Bernstein)
* Performance was improved.
* Threshold date for messages age can be now specified as an
  offset (in days) from today.
* It's possible to specify in which directory archive mailbox
  must be saved.
* Archive mailbox's extension can be customized.
* Install script has been changed: archmbox is now linked to
  /usr/bin/archmbox
		  
release 0.2.0:

* The default name of the archive mailbox was changed.
* Archived messages are now appended to the archive mailbox
  to allow multiple executions of the script against the same
  mailbox.
* Date must be now specified in the format yyyy-mm-dd.

release 0.1.5:

* minor output improvements.

release 0.1.4:

* install script added.

release 0.1.3:

* help informations improved.

release 0.1.2:

* fixed some minor errors.

release 0.1.1:

* fixed some minor errors.

release 0.1.0:

* first public release.
